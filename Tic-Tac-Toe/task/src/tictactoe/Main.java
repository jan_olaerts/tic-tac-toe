package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        // write your code here
        char[][] input = printEmptyCells();
        enterCoordinates(input);
    }

    public static char[][] printEmptyCells() {
        char[][] emptyCells = new char[][] {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
        System.out.println("---------");
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                if(j == 0) System.out.print("| " + emptyCells[i][j]);
                if(j == 2) System.out.print(emptyCells[i][j] + " |" + "\n");
                if(j != 0 && j != 2) System.out.print(" " + emptyCells[i][j] + " ");
            }
        }
        System.out.println("---------");

        return emptyCells;
    }

    public static void enterCoordinates(char[][] input) {

        boolean impossibleWinDraw = false;
        int count = 1;
        while(!impossibleWinDraw) {

            System.out.print("Enter the coordinates: ");

            int c1 = 0;
            int c2 = 0;
            try {
                c1 = scanner.nextInt();
                c2 = scanner.nextInt();
            } catch(InputMismatchException e) {
                System.out.println("You should enter numbers!");
                scanner.nextLine();
                continue;
            }


            if(c1 < 1 || c1 > 3 || c2 < 1 || c2 > 3) {
                System.out.println("Coordinates should be from 1 to 3!");
                continue;
            }

            int i = 0;
            int j = 0;
            if(c2 == 3) i = 0;
            else if(c2 == 2) i = 1;
            else if(c2 == 1) i = 2;

            switch(c1) {
                case 1:
                    j = 0;
                    break;
                case 2:
                    j = 1;
                    break;
                case 3:
                    j = 2;
                    break;

            }

            if(input[i][j] == 'X' || input[i][j] == 'O') {
                System.out.println("This cell is occupied! Choose another one!");
                continue;
            } else {
                input[i][j] = count % 2 == 0 ? 'O' : 'X';
                count++;
            }

            printOutput(input);
            impossibleWinDraw = checkState(input);
        }
    }

    public static void printOutput(char[][] input) {
        System.out.println("---------");
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {

                if(input[i][j] == '_') input[i][j] = ' ';
                if(j == 0) System.out.print("| " + input[i][j]);
                if(j == 2) System.out.print(input[i][j] + " |" + "\n");
                if(j != 0 && j != 2) System.out.print(" " + input[i][j] + " ");
            }
        }
        System.out.println("---------");
    }

    public static boolean checkState(char[][] input) {

        int xAmount = 0;
        int oAmount = 0;
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                if(input[i][j] == 'X') {
                    xAmount++;
                } else if (input[i][j] == 'O') {
                    oAmount++;
                }
            }
        }

        int threeX = 0;
        int threeO = 0;
        for(int i = 0; i < 3; i++) {

            for(int j = 0; j < 3; j++) {

                // Horizontal check
                if(j == 0) {
                    if(input[i][j] == input[i][j+1] && input[i][j] == input[i][j+2]) {
                        if(input[i][j] == 'X') threeX++;
                        if(input[i][j] == 'O') threeO++;
                    }
                }

                // Vertical check
                if(i == 0) {
                    if(input[i][j] == input[i+1][j] && input[i][j] == input[i+2][j]) {
                        if(input[i][j] == 'X') threeX++;
                        if(input[i][j] == 'O') threeO++;
                    }
                }

                // Diagonal check
                if(i == 0 && j == 0) {
                    if(input[i][j] == input[i+1][j+1] && input[i][j] == input[i+2][j+2]) {
                        if(input[i][j] == 'X') threeX++;
                        if(input[i][j] == 'O') threeO++;
                    }
                }

                // Diagonal check
                if(i == 0 && j == 2) {
                    if(input[i][j] == input[i+1][j-1] && input[i][j] == input[i+2][j-2]) {
                        if(input[i][j] == 'X') threeX++;
                        if(input[i][j] == 'O') threeO++;
                    }
                }
            }
        }

        if(xAmount > oAmount+1 || oAmount > xAmount+1) {
            System.out.println("Impossible");
            return true;
        } else if(threeX == 1 && threeO == 0) {
            System.out.println("X wins");
            return true;
        } else if(threeO == 1 && threeX == 0) {
            System.out.println("O wins");
            return true;
        } else if(threeX == 1 && threeO == 1 || (xAmount == 5 && oAmount == 4)) {
            System.out.println("Draw");
            return true;
        }

        return false;
    }
}